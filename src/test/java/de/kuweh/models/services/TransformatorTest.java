package de.kuweh.models.services;

import static org.junit.Assert.*;

import org.junit.Test;

import de.kuweh.models.services.Transformator;

public class TransformatorTest {

	@Test
	public void testSimpleObject() {
		Object input = new Object();
		
		Transformator<Object, Object> transformator = new Transformator<>();
		transformator.setInputInstance(input);
		
		assertEquals(input.getClass(), transformator.getInputInstance().getClass());
	}
	
	@Test
	public void testPostFormObject() throws TransferException {
		
		de.kuweh.models.view.PostForm view = new de.kuweh.models.view.PostForm();
		view.setTitle("test");
		view.setText("test text");
		
		de.kuweh.models.dao.Post dao = new de.kuweh.models.dao.Post();
		
		Transformator<de.kuweh.models.view.PostForm, de.kuweh.models.dao.Post> transformator = new Transformator<>();
		transformator.setInputInstance(view);
		transformator.setOutputInstance(dao);
		transformator.fillOutputInstance();
		
		assertEquals(view.getClass(), transformator.getInputInstance().getClass());
		assertEquals(dao.getClass(), transformator.getOutputInstance().getClass());
		assertEquals("test", dao.getTitle());
		assertEquals("test text", dao.getText());
	}
	
	@Test
	public void testPostJsonObject() throws TransferException {
		
		de.kuweh.models.view.PostJson json = new de.kuweh.models.view.PostJson();
		json.setId((long)1);
		json.setTitle("test");
		json.setText("test text");
		
		de.kuweh.models.dao.Post dao = new de.kuweh.models.dao.Post();
		
		Transformator<de.kuweh.models.view.PostJson, de.kuweh.models.dao.Post> transformator = new Transformator<>();
		transformator.setInputInstance(json);
		transformator.setOutputInstance(dao);
		transformator.fillOutputInstance();
		
		assertEquals(json.getClass(), transformator.getInputInstance().getClass());
		assertEquals(dao.getClass(), transformator.getOutputInstance().getClass());
		assertEquals((long)1, 		dao.getId());
		assertEquals("test", 		dao.getTitle());
		assertEquals("test text", 	dao.getText());
		
		de.kuweh.models.view.PostJson json2 = new de.kuweh.models.view.PostJson();
		
		Transformator<de.kuweh.models.dao.Post, de.kuweh.models.view.PostJson> transformator2 = new Transformator<>();
		transformator2.setInputInstance(dao);
		transformator2.setOutputInstance(json2);
		transformator2.fillOutputInstance();
		
		assertEquals(dao.getClass(), transformator2.getInputInstance().getClass());
		assertEquals(json2.getClass(), transformator2.getOutputInstance().getClass());
		assertEquals((long)1, 		json2.getId());
		assertEquals("test", 		json2.getTitle());
		assertEquals("test text", 	json2.getText());
	}

}
