package de.kuweh.models.services;

import static org.junit.Assert.*;

import org.junit.Test;

import de.kuweh.models.dao.Post;
import de.kuweh.models.view.PostForm;

public class PostTransformatorFactoryTest {

	@Test
	public void testDaoTransformator() {
		PostTransformatorFactory factory = new PostTransformatorFactory();
		Transformator<PostForm, Post> transform = factory.makeDaoTransformator();
		
		assertNotNull(transform);
		assertEquals(Transformator.class, transform.getClass());
	}
	
	@Test
	public void testFormTransformator() {
		PostTransformatorFactory factory = new PostTransformatorFactory();
		
		assertNotNull(factory.makeFormTransformator());
	}
	
	@Test
	public void testJsonTransformator() {
		PostTransformatorFactory factory = new PostTransformatorFactory();
		
		assertNotNull(factory.makeJsonTransformator());
	}

}
