package de.kuweh.models.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Test;

import de.kuweh.dao.repositories.PostRepository;
import de.kuweh.models.dao.Post;
import de.kuweh.models.view.PostForm;
import de.kuweh.models.view.PostJson;

public class PostServiceTest {

	@Test
	public void testNewSercice() {
		
		PostRepository repo = mock(PostRepository.class);
		PostService service = new PostService(repo, new PostTransformatorFactory());
		
		assertNotNull(service);
		assertEquals(repo, service.getRepository());
		assertNotNull(service.getDaoTransformator());
		assertNotNull(service.getFormTransformator());
		assertNotNull(service.getJsonTransformator());
	}
	
	@Test
	public void testSave() {
		
		PostRepository repo 		= mock(PostRepository.class);
		PostForm form 				= new PostForm("Test Title", "Test Text");
		
		PostService service 		= new PostService(repo, new PostTransformatorFactory());
		service.save(form);
	}
	
	@Test
	public void testGetForm() {
		
		PostRepository repo 		= mock(PostRepository.class);
		PostService service 		= new PostService(repo, new PostTransformatorFactory());
		PostForm form 				= service.getForm();
		
		assertNotNull(form);
		
		Long id 					= new Long(1);
		form 						= service.getForm(id);
		
		assertNotNull(form);
	}
	
	@Test
	public void testGetJson() {
		
		Long id 					= new Long(1);
		Post post					= new Post((long)id);
		post.setTitle("Test");
		post.setText("Test text");
		
		assertEquals((long)id, 		post.getId());
		
		PostRepository repo = mock(PostRepository.class);
		when(repo.findOne(any())).thenReturn(post);
		
		PostService service 		= new PostService(repo, new PostTransformatorFactory());
		PostJson json 				= service.getJson();
		
		assertNotNull(json);
		
		json 						= service.getJson(id);
		
		assertNotNull(json);
		assertEquals((long)id, 		json.getId());
		assertEquals("Test", 		json.getTitle());
		assertEquals("Test text", 	json.getText());
	}

	@Test
	public void testGetList() {
		
		PostRepository repo 		= mock(PostRepository.class);
		PostService service 		= new PostService(repo, new PostTransformatorFactory());
		ArrayList<PostJson> list 	= service.getJsonList();
		
		assertNotNull(list);
	}
}
