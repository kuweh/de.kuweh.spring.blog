package de.kuweh.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.kuweh.dao.repositories.PostRepository;
import de.kuweh.dao.repositories.UserRepository;
import de.kuweh.models.services.PostService;
import de.kuweh.models.services.PostTransformatorFactory;
import de.kuweh.models.services.UserService;
import de.kuweh.models.services.UserTransformatorFactory;

@Configuration
public class ServiceConfig {
    
    @Autowired
    private PostRepository postRepo;
    
    @Autowired
    private UserRepository userRepo;
    
    @Bean
    public PostService postSerivce() {

        UserService userService = this.userSerivce();
        
        PostTransformatorFactory transformatorFactory = new PostTransformatorFactory();
        PostService service = new PostService(postRepo, transformatorFactory);
        service.setUserService(userService);
        
        return service;
    }
    
    @Bean
    public UserService userSerivce() {

        UserTransformatorFactory transformatorFactory = new UserTransformatorFactory();
        UserService service = new UserService(userRepo, transformatorFactory);
        
        return service;
    }
}
