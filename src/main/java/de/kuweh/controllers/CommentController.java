package de.kuweh.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import de.kuweh.dao.repositories.CommentRepository;
import de.kuweh.models.dao.Comment;

@Controller
@RequestMapping("/comments")
public class CommentController {

	@Autowired
	private CommentRepository commentRepo;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public @ResponseBody Iterable<Comment> listAllComments() {
		return commentRepo.findAll();
	}
}
