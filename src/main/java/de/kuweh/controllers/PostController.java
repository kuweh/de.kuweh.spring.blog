package de.kuweh.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import de.kuweh.models.dao.Post;
import de.kuweh.models.services.PostService;
import de.kuweh.models.view.PostForm;

@Controller
@RequestMapping("/posts")
public class PostController {
	
	@Autowired
	private PostService postService;

	@RequestMapping(value="", method=RequestMethod.GET)
	public @ResponseBody Iterable<Post> listAllPosts() {
		return postService.getRepository().findAll();
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.GET)
    public @ResponseBody Post getPost(@PathVariable("id") long id) {
        return postService.getRepository().findOne(id);
    }
	
	@RequestMapping(value="", method=RequestMethod.POST)
    public @ResponseBody ResponseEntity<Void> addNewPost(@Valid @RequestBody PostForm postform, UriComponentsBuilder ucBuilder) {
	    
	    Post post = postService.save(postform);
	    
	    HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/posts/{id}").buildAndExpand(post.getId()).toUri());
        
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
	
	@RequestMapping(value="{id}", method=RequestMethod.PATCH)
    public @ResponseBody ResponseEntity<Void> updatePost(@PathVariable("id") long id, @Valid @RequestBody PostForm postform, UriComponentsBuilder ucBuilder) {
        
	    Post post = postService.getRepository().findOne(id);
	    
	    if (post == null) {
	        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	    
        post = postService.update(post, postform);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/posts/{id}").buildAndExpand(post.getId()).toUri());
        
        return new ResponseEntity<Void>(headers, HttpStatus.NO_CONTENT);
    }
}
