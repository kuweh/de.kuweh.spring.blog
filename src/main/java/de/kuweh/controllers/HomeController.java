package de.kuweh.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import de.kuweh.dao.repositories.PostRepositoryImpl;
import de.kuweh.models.dao.Post;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private PostRepositoryImpl postRepo;

    @RequestMapping(value="", method=RequestMethod.GET)
    public @ResponseBody Post getLatestPosts() {
        return postRepo.getLastPost();
    }
}
