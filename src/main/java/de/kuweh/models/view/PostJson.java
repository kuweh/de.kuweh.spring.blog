package de.kuweh.models.view;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonView;

public class PostJson extends BaseJsonModel {

	@JsonView(ViewJson.Base.class)
	private String title;

	@JsonView(ViewJson.Base.class)
	private String text;
	
	@JsonView(ViewJson.Base.class)
	private String author;
	
	@JsonView(ViewJson.Base.class)
	private Date created;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
}
