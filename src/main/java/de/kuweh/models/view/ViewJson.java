package de.kuweh.models.view;

/**
 * Json view definition
 * 
 * @author Kai Hempel
 */
public class ViewJson {
	interface Base {}
	interface Detailed {}
}
