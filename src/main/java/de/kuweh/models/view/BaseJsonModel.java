package de.kuweh.models.view;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Base json model
 * 
 * @author Kai Hempel
 */
public class BaseJsonModel {

	/**
	 * The data id
	 */
	@JsonView(ViewJson.Base.class)
	protected long id;
	
	/**
	 * Returns the data ID
	 * 
	 * @return
	 */
	public long getId() {
		return this.id;
	}
	
	/**
	 * Sets the data ID
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
}
