package de.kuweh.models.view;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PostForm extends BaseFormModel {

	@NotNull
	@Size(min = 1, max = 100)
	private String title;

	@NotNull
	@Size(min = 1, max = 10000)
	private String text;

	public PostForm() {
	}

	public PostForm(String title, String text) {
		this.title = title;
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}