package de.kuweh.models.dao;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user")
public class User extends BaseDaoModel {

	@NotNull
	@Size(min = 3, max = 100)
	private String email;
	
	@NotNull
	@Size(min = 6, max = 100)
	private String password;
	
	@NotNull
	@Size(min = 1, max = 100)
	private String firstname;
	
	@NotNull
	@Size(min = 1, max = 100)
	private String lastname;
	
	@NotNull
	@Size(min = 1, max = 100)
	private String username;
	
	public User() {}
	
	public User(long id) {
		this.id = id;
	}
	
	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
}
