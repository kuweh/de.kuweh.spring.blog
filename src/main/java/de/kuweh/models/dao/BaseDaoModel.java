package de.kuweh.models.dao;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Basic data access object
 * 
 * @author Kai Hempel
 */
@MappedSuperclass
public class BaseDaoModel {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected long id;
	
	/**
	 * Return the data ID
	 * 
	 * @return Database primary key
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Set the data ID
	 * 
	 * @return void
	 */
	public void setId(long id) {
		this.id = id;
	}
}
