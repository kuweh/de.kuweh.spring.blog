package de.kuweh.models.dao;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.kuweh.models.dao.Post;
import de.kuweh.models.dao.User;

@Entity
@Table(name = "comment")
@JsonIgnoreProperties({"JavassistLazyInitializer"})
public class Comment extends BaseDaoModel {

	@ManyToOne(targetEntity=Post.class, optional=false)
	@JoinColumn(name = "post_id", nullable=false, updatable=false)
	private Post post;
	
	@ManyToOne(targetEntity=User.class, optional=false)
    @JoinColumn(name = "user_id", nullable=false, updatable=false)
	private User user;
	
	@NotNull
	@Size(min = 1, max = 50)
	private String title;

	@NotNull
	@Size(min = 1, max = 1000)
	private String text;
	
	@NotNull
	@DateTimeFormat
	private Date created;
	
	@NotNull
	@DateTimeFormat
	private Date updated;
	
	public Comment() {
	}

	public Comment(long id) {
		this.id = id;
	}

	public Comment(String title, String text) {
		this.title = title;
		this.text = text;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
