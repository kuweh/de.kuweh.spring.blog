package de.kuweh.models.dao;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "post")
public class Post extends BaseDaoModel {

	@NotNull
	@Size(min = 1, max = 100)
	private String author;

	@NotNull
	@Size(min = 1, max = 100)
	private String title;

	@NotNull
	@Size(min = 1, max = 10000)
	private String text;
	
	@NotNull
	@DateTimeFormat
	private Date created;
	
	@NotNull
	@DateTimeFormat
	private Date updated;

	public Post() {
	}

	public Post(long id) {
		this.setId(id);
	}

	public Post(String author, String title, String text) {
		this.setAuthor(author);
		this.setTitle(title);
		this.setText(text);
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public Date getCreated() {
		return this.created;
	}
	
	public void setCreated(Date created) {
		this.created = created;
	}
	
	public Date getUpdated() {
		return this.updated;
	}
	
	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}