package de.kuweh.models.services;

import de.kuweh.models.dao.User;
import de.kuweh.models.view.UserForm;
import de.kuweh.models.view.UserJson;

public class UserTransformatorFactory extends AbstractTransformatorFactory {

    @Override
    protected Transformator<UserForm, User> makeDaoTransformator() {
        return new Transformator<UserForm, User>();
    }

    @Override
    protected Transformator<User, UserForm> makeFormTransformator() {
        return new Transformator<User, UserForm>();
    }

    @Override
    protected Transformator<User, UserJson> makeJsonTransformator() {
        return new Transformator<User, UserJson>();
    }

}
