package de.kuweh.models.services;

import de.kuweh.models.dao.Post;
import de.kuweh.models.view.PostForm;
import de.kuweh.models.view.PostJson;

/**
 * Post transformator factory
 * 
 * @author Kai Hempel
 */
public class PostTransformatorFactory extends AbstractTransformatorFactory {

	/**
	 * Create a form to post transformator
	 * 
	 * @return Transformator instance
	 */
	@Override
	protected Transformator<PostForm, Post> makeDaoTransformator() {
		return new Transformator<PostForm, Post>();
	}

	/**
	 * Create a post to form transformator
	 * 
	 * @return Transformator instance
	 */
	@Override
	protected Transformator<Post, PostForm> makeFormTransformator() {
		return new Transformator<Post, PostForm>();
	}

	/**
	 * Create a post to json transformator
	 * 
	 * @return Transformator instance
	 */
	@Override
	protected Transformator<Post, PostJson> makeJsonTransformator() {
		return new Transformator<Post, PostJson>();
	}

}
