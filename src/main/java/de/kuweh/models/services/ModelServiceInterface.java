package de.kuweh.models.services;

import de.kuweh.models.dao.BaseDaoModel;
import de.kuweh.models.view.BaseFormModel;
import de.kuweh.models.view.BaseJsonModel;

interface ModelServiceInterface {

	/**
	 * Persists the data
	 * 
	 * @param modelData
	 * @return
	 */
	public BaseDaoModel save(BaseFormModel modelData);
	
	/**
	 * Update the data
	 * 
	 * @param model
	 * @param modelData
	 * @return
	 */
	public BaseDaoModel update(BaseDaoModel model, BaseFormModel modelData);
	
	/**
	 * Returns the form model
	 * 
	 * @return
	 */
	public BaseFormModel getForm();
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public BaseFormModel getForm(Long id);
	
	/**
	 * Returns the json model
	 * 
	 * @return
	 */
	public BaseJsonModel getJson();
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public BaseJsonModel getJson(Long id);
}
