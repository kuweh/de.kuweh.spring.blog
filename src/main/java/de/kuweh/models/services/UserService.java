package de.kuweh.models.services;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

import de.kuweh.models.dao.BaseDaoModel;
import de.kuweh.models.dao.User;
import de.kuweh.models.view.BaseFormModel;
import de.kuweh.models.view.BaseJsonModel;
import de.kuweh.models.view.UserForm;
import de.kuweh.models.view.UserJson;

/**
 * User service
 * 
 * @author Kai Hempel
 */
public class UserService extends BaseService implements ModelServiceInterface, ModelListServiceInterface {

    public UserService(CrudRepository<User, Long> repository, UserTransformatorFactory factory) {
        super(repository, factory);
    }

    /**
     * Returns the repository
     * 
     * @return The crud repository
     */
    @SuppressWarnings("unchecked")
    @Override
    public CrudRepository<User, Long> getRepository() {
        return (CrudRepository<User, Long>) repository;
    }
    
    /**
     * Returns the transformator factory
     * 
     * @return UserTransformatorFactory
     */
    @Override
    public UserTransformatorFactory getFactory() {
        return (UserTransformatorFactory)this.factory;
    }

    /**
     * Returns the DAO transformator
     * 
     * @return Transformator
     */
    @Override
    protected Transformator<UserForm, User> getDaoTransformator() {
        return getFactory().makeDaoTransformator();
    }

    /**
     * Returns the Form transformator
     * 
     * @return Transformator
     */
    @Override
    protected Transformator<User, UserForm> getFormTransformator() {
        return getFactory().makeFormTransformator();
    }
    
    /**
     * Returns the Json transformator
     * 
     * @return Transformator
     */
    @Override
    protected Transformator<User, UserJson> getJsonTransformator() {
        return getFactory().makeJsonTransformator();
    }

    
    @Override
    public ArrayList<?> getJsonList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BaseDaoModel save(BaseFormModel modelData) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public BaseDaoModel update(BaseDaoModel model, BaseFormModel modelData) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BaseFormModel getForm() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BaseFormModel getForm(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BaseJsonModel getJson() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public BaseJsonModel getJson(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    public User getLoginUser() {
        
        // !TODO: Implement real detection
        
        // Create mock user
        User user = new User();
        user.setEmail("test@test.de");
        user.setFirstname("Toni");
        user.setLastname("Tester");
        
        return user;
    }

}
