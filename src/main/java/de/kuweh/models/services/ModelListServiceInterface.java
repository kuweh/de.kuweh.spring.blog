package de.kuweh.models.services;

import java.util.ArrayList;

/**
 * Model list interface
 * 
 * @author Kai Hempel
 */
public interface ModelListServiceInterface {

	/**
	 * Returns a json model list for API response
	 *  
	 * @return List of json model objects
	 */
	public ArrayList<?> getJsonList();
}
