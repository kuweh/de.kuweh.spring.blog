package de.kuweh.models.services;

/**
 * Creates the typ depending transformator
 * 
 * @author Kai Hempel
 */
abstract public class AbstractTransformatorFactory {
	
	/**
	 * Returns the form to dao transformator
	 * 
	 * @return Dao transformator
	 */
	abstract protected Transformator<?, ?> makeDaoTransformator();
	
	/**
	 * Returns the form transformator
	 * 
	 * @return Form transformator
	 */
	abstract protected Transformator<?, ?> makeFormTransformator();
	
	/**
	 * Returns the dao to json transformator
	 * 
	 * @return Json transformator
	 */
	abstract protected Transformator<?, ?> makeJsonTransformator();
}
