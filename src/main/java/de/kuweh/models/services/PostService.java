package de.kuweh.models.services;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.repository.CrudRepository;

import de.kuweh.models.dao.BaseDaoModel;
import de.kuweh.models.dao.Post;
import de.kuweh.models.dao.User;
import de.kuweh.models.services.utils.PostUtils;
import de.kuweh.models.view.BaseFormModel;
import de.kuweh.models.view.PostForm;
import de.kuweh.models.view.PostJson;

/**
 * Post service
 * 
 * @author Kai Hempel
 */
public class PostService extends BaseService implements ModelServiceInterface, ModelListServiceInterface {

    private UserService userService;
    
    /**
	 * Constructor
	 * 
	 * @param repository
	 */
	public PostService(CrudRepository<Post, Long> repository, PostTransformatorFactory factory) {
		super(repository, factory);
	}
	
	/**
	 * Returns the user service
	 * 
	 * @return UserService
	 */
	public UserService getUserService() {
        return userService;
    }

	/**
	 * Sets the user service
	 * 
	 * @param userService
	 */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
	
	/**
	 * Returns the repository
	 * 
	 * @return The crud repository
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CrudRepository<Post, Long> getRepository() {
		return (CrudRepository<Post, Long>) repository;
	}
	
	/**
	 * Returns the transformator factory
	 * 
	 * @return Post transformator factory
	 */
	@Override
	public PostTransformatorFactory getFactory() {
		return (PostTransformatorFactory)this.factory;
	}

	/**
	 * Returns the DAO transformator
	 * 
	 * @return Transformator
	 */
	@Override
	protected Transformator<PostForm, Post> getDaoTransformator() {
		return getFactory().makeDaoTransformator();
	}

	/**
	 * Returns the Form transformator
	 * 
	 * @return Transformator
	 */
	@Override
	protected Transformator<Post, PostForm> getFormTransformator() {
		return getFactory().makeFormTransformator();
	}
	
	/**
	 * Returns the Json transformator
	 * 
	 * @return Transformator
	 */
	@Override
	protected Transformator<Post, PostJson> getJsonTransformator() {
		return getFactory().makeJsonTransformator();
	}

	/**
	 * Saves the form data into database
	 * 
	 * @return void
	 */
	@Override
	public Post save(BaseFormModel modelData) {
		
		Post post = new Post();
		
		transformFormIntoDao((PostForm) modelData, post);
		addAuthorNameToPost(post);
		addTimestampsToPost(post);
		
		return getRepository().save(post);
	}
	
	/**
	 * 
	 */
	@Override
	public Post update(BaseDaoModel model, BaseFormModel modelData) {
	    
	    Post post = (Post) model;
	    
	    transformFormIntoDao((PostForm) modelData, post);
	    addTimestampsToPost(post);
	    
	    return getRepository().save(post);
	}
	
	/**
	 * Transfers the form data into the dao instance
	 * 
	 * @param form
	 * @param post
	 */
	private void transformFormIntoDao(PostForm form, Post post) {
	    
	    try {
	        getDaoTransformator()
	            .setInputInstance(form)
	            .setOutputInstance(post)
                .fillOutputInstance();
            
        } catch (TransferException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * Add the full user name as author to the post
	 * 
	 * @param post
	 */
	private void addAuthorNameToPost(Post post) {
	    
	    User user = userService.getLoginUser();
	    String author = user.getFirstname() + " " + user.getLastname();
	    
	    post.setAuthor(author);
	}
	
	/**
	 * Add the timestamps to the new entry
	 * 
	 * @param post
	 */
	private void addTimestampsToPost(Post post) {
	    Date currentDate = new Date();
	    
	    if (post.getCreated() == null) {
	        post.setCreated(currentDate);
	    }
	    
	    post.setUpdated(currentDate);
	}

	/**
	 * Returns a new form view object
	 * 
	 * @return Form view instance
	 */
	@Override
	public PostForm getForm() {
		return new PostForm();
	}
	
	/**
	 * Returns a filled form instance 
	 * 
	 * @return Form view instance
	 */
	@Override
	public PostForm getForm(Long id) {
	    
		Post post = getRepository().findOne(id);
		return fillForm(post);
	}
	
	/**
	 * Fills the form instance with data
	 * 
	 * @param post
	 * @return
	 */
	private PostForm fillForm(Post post) {
	    
	    try {
	        
	        Transformator<Post, PostForm> transformator = getFormTransformator();
	        
	        transformator.setInputInstance(post)
                .setOutputInstance(getForm())
                .fillOutputInstance();
	        
	        return transformator.getOutputInstance();
            
        } catch (TransferException e) {
            e.printStackTrace();
        }
	    
        return getForm();
	}

	/**
	 * Returns a new json view object
	 * 
	 * @return json view instance
	 */
	@Override
	public PostJson getJson() {
		return new PostJson();
	}
	
	/**
	 * Returns the post entry for the given id as json view object
	 * 
	 * @param 	Long id
	 * @return 	json view instance
	 */
	@Override
	public PostJson getJson(Long id) {
		
		PostJson json = new PostJson();
		Post post = getRepository().findOne(id);
		Transformator<Post, PostJson> transformator = getJsonTransformator();
		
		try {
			transformator.setInputInstance(post)
						 .setOutputInstance(json)
						 .fillOutputInstance();
			
		} catch (TransferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return json;
	}
	
	/**
	 * Returns a list of post json instances
	 * 
	 * @return List of post entries
	 */
	@Override
	public ArrayList<PostJson> getJsonList() {
		
		ArrayList<PostJson> list = new ArrayList<>();
		Iterable<Post> postList = getRepository().findAll();
		
		if (postList != null) {
			postList.forEach(post -> PostUtils.fillListWithJson(getJsonTransformator(), list, post));
		}
		
		return list;
	}
	
}
