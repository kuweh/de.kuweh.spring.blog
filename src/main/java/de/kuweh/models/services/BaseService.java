package de.kuweh.models.services;

import org.springframework.data.repository.CrudRepository;

import de.kuweh.models.dao.BaseDaoModel;
import de.kuweh.models.view.BaseFormModel;
import de.kuweh.models.view.BaseJsonModel;

/**
 * Model service
 * 
 * @author Kai Hempel
 */
abstract class BaseService {

	/**
	 * Repository
	 */
	protected CrudRepository<?, ?> repository;
	
	/**
	 * Transformator factory
	 */
	protected AbstractTransformatorFactory factory;
	
	/**
	 * Form model
	 */
	protected BaseFormModel form;
	
	/**
	 * Json model
	 */
	protected BaseJsonModel json;
	
	/**
	 * Data access model
	 */
	protected BaseDaoModel dao;
	
	/**
	 * Constructor
	 * 
	 * @param repository
	 */
	public BaseService(CrudRepository<?, ?> repository, AbstractTransformatorFactory factory) {
		this.setRepository(repository);
		this.setFactory(factory);
	}
	
	/**
	 * Sets the repository
	 * 
	 * @param repository
	 */
	public void setRepository(CrudRepository<?, ?> repository) {
		this.repository = repository;
	}
	
	/**
	 * Returns the current repository
	 * 
	 * @return
	 */
	abstract public CrudRepository<?, ?> getRepository();
	
	/**
	 * 
	 * @param factory
	 */
	public void setFactory(AbstractTransformatorFactory factory) {
		this.factory = factory;
	}
	
	/**
	 * Returns the transformator factory
	 * 
	 * @return
	 */
	abstract public AbstractTransformatorFactory getFactory();
	
	/**
	 * Returns the form to dao transformator
	 * 
	 * @return Dao transformator
	 */
	abstract protected Transformator<?, ?> getDaoTransformator();
	
	/**
	 * Returns the form transformator
	 * 
	 * @return Form transformator
	 */
	abstract protected Transformator<?, ?> getFormTransformator();
	
	/**
	 * Returns the dao to json transformator
	 * 
	 * @return Json transformator
	 */
	abstract protected Transformator<?, ?> getJsonTransformator();

}
