package de.kuweh.models.services.utils;

import java.util.ArrayList;

import de.kuweh.models.dao.Post;
import de.kuweh.models.services.TransferException;
import de.kuweh.models.services.Transformator;
import de.kuweh.models.view.PostJson;

public class PostUtils {
	
	public static void fillListWithJson(Transformator<Post, PostJson> transformator, ArrayList<PostJson> list, Post post) {
		
		PostJson json = new PostJson();
		
		try {
			transformator.setInputInstance(post)
			 			 .setOutputInstance(json)
			 			 .fillOutputInstance();
			
		} catch (TransferException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		list.add(json);
	}
}
