package de.kuweh.dao.repositories;

import de.kuweh.models.dao.Post;

public interface PostRepositoryExtentsion {
    
    public Post getLastPost();
}
