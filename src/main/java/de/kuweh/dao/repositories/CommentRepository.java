package de.kuweh.dao.repositories;

import org.springframework.data.repository.CrudRepository;
import de.kuweh.models.dao.Comment;

public interface CommentRepository extends CrudRepository<Comment, Long> {

}
