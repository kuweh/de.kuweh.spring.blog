package de.kuweh.dao.repositories;

import org.springframework.data.repository.CrudRepository;
import de.kuweh.models.dao.Post;

public interface PostRepository extends CrudRepository<Post, Long>, PostRepositoryExtentsion {

}
