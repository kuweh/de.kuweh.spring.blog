package de.kuweh.dao.repositories;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import de.kuweh.models.dao.Post;

public class PostRepositoryImpl implements PostRepositoryExtentsion {
    
    @PersistenceContext 
    private EntityManager em;
    
    @Override
    public Post getLastPost() {
        String sql = "SELECT * FROM post ORDER BY post.created DESC LIMIT 1";
        Post post = new Post();
        
        try {
            Query query = em.createNativeQuery(sql, Post.class);
            return (Post) query.getSingleResult();
            
        } catch (PersistenceException e) {
            // Ignore and return empty instance
        }
       
        return post;
    }

}
