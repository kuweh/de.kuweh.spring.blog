package de.kuweh.dao.repositories;

import org.springframework.data.repository.CrudRepository;

import de.kuweh.models.dao.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
